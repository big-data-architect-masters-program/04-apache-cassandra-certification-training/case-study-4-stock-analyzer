# Case Study 4 STOCK ANALYZER (in progress)

We have completed module 2 and Now you must have got a basic understanding of Cassandra’s Data Model. Spend some time to figure out below-mentioned use case to know why and how you can leverage Cassandra.  

## Statement:

The following figure shows a simple relational data model of a stock quote application:  
![Simple Relational Data Model](img/img-01.png)

The stock_symbol table is representing the stock master information such as the *symbol* of a stock, the *description* of the stock, and the *exchange* that the stock is traded. The stock_ticker table is storing the prices of *open, high, low, close,* and the *transacted volume of a stock* on a trading day. The two tables have a relationship based on the *symbol column*.

## Task: 

1. Provide a data model, using CQL create table syntax which will enable the optimized search result for the following query:  
A. All the daily close prices and descriptions of the stocks listed in the NASDAQ
exchange.  
B. All day close prices and descriptions of the stocks listed in the NASDAQ
exchange on November 21, 2017.  

### Constraints:

1. A row cannot be split across two nodes.
2. Try to minimize network travel through the data model: In a distributed system backed by Cassandra, we should minimize unnecessary network traffic as much possible. In other words, the lesser the number of nodes the query needs to work with, the better the performance of the data model.
